﻿using System;
using System.Reflection;

namespace OtusHomework.Reflection
{
    public static class MemberInfoExtensions
    {
        public static object GetValue(this MemberInfo memberInfo, object obj)
        {
            switch (memberInfo.MemberType)
            {
                case MemberTypes.Property:
                    return ((PropertyInfo) memberInfo).GetValue(obj);
                case MemberTypes.Field:
                    return ((FieldInfo) memberInfo).GetValue(obj);
                default:
                    throw new ArgumentOutOfRangeException($"Unexpected member type: {memberInfo.MemberType}"); 
            }
        }
        
        public static void SetValue(this MemberInfo memberInfo, object obj, object value)
        {
            switch (memberInfo.MemberType)
            {
                case MemberTypes.Property:
                    ((PropertyInfo) memberInfo).SetValue(obj, value);
                    break;
                case MemberTypes.Field:
                    ((FieldInfo) memberInfo).SetValue(obj, value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Unexpected member type: {memberInfo.MemberType}"); 
            }
        }
    }
}