﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace OtusHomework.Reflection
{
    public static class Serializer
    {
        private const string Delimiter = ", ";

        #region Serialization
        
        public static string SerializeFromObjectToCSV(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            
            var builder = new StringBuilder();
            SerializeObject(builder, obj);

            return builder.ToString();
        }

        private static void SerializeObject(StringBuilder builder, object obj)
        {
            var type = obj.GetType();
            var members = GetMembersToSerialization(type);
            
            AppendType(builder, type);
            AppendNames(builder, members);
            AppendValues(builder, members, obj);
        }

        private static void AppendType(StringBuilder builder, Type type)
        {
            builder.AppendLine(type.ToString());
        }

        private static void AppendNames(StringBuilder builder, MemberInfo[] members)
        {
            for (var i = 0; i < members.Length; i++)
            {
                AppendValue(builder, members[i].Name
                    , IsLastIteration(members.Length, i));
            }
            
            builder.AppendLine();
        }

        private static void AppendValues(StringBuilder builder, MemberInfo[] members, object obj)
        {
            for (var i = 0; i < members.Length; i++)
            {
                AppendValue(builder, members[i].GetValue(obj)?.ToString()
                    , IsLastIteration(members.Length, i));
            }
        }

        private static void AppendValue(StringBuilder builder, string value, bool lastValue)
        {
            builder.Append(value);

            if (!lastValue)
            {
                builder.Append(Delimiter);
            }
        }

        private static bool IsLastIteration(int iterationCount, int currentIteration) =>
            iterationCount - currentIteration <= 1;

        private static MemberInfo[] GetMembersToSerialization(Type type)
        {
            return type.FindMembers(MemberTypes.Property | MemberTypes.Field
                , BindingFlags.Instance | BindingFlags.Public, null, null);
        }

        #endregion

        #region Deserialization

        public static object DeserializeFromCSVToObject(string csv)
        {
            if (csv == null)
            {
                throw new ArgumentNullException(nameof(csv));
            }

            var lines = GetCsvLines(csv);
            var type = GetTypeByCsvLine(lines[0]);
            var names = lines[1].Split(Delimiter);
            var values = lines[2].Split(Delimiter);
            var nameValueDict = ConvertNamesAndValuesToDictionary(names, values);
            
            return CreateObjectByValues(type, nameValueDict);
        }

        private static string[] GetCsvLines(string csv)
        {
            const int minCsvLines = 3;

            var lines = csv.Split("\r\n");

            if (lines.Length < minCsvLines)
            {
                throw new InvalidCsvFormatException();
            }

            return lines;
        }

        private static Type GetTypeByCsvLine(string line)
        {
            var type = Type.GetType(line);
            
            if (type == null)
            {
                throw new InvalidCsvFormatException();
            }

            return type;
        }

        private static IDictionary<string, string> ConvertNamesAndValuesToDictionary(string[] names, string[] values)
        {
            if (names.Length != values.Length)
            {
                throw new InvalidCsvFormatException(); 
            }
            
            var nameValueDict = new Dictionary<string, string>();

            for (var i = 0; i < names.Length; i++)
            {
                nameValueDict[names[i]] = values[i];
            }

            return nameValueDict;
        }

        private static object CreateObjectByValues(Type type, IDictionary<string, string> nameValueDict)
        {
            var obj = Activator.CreateInstance(type);
            var members = GetMembersToSerialization(type);

            foreach (var member in members)
            {
                var canWrite = (member as PropertyInfo)?.CanWrite ?? true;
                
                if (!canWrite)
                {
                    continue;
                }

                if (!nameValueDict.TryGetValue(member.Name, out var value))
                {
                    throw new InvalidCsvFormatException();
                }
                
                var memberType = member.GetValue(obj)?.GetType();
                
                member.SetValue(obj, memberType == null ? null : Convert.ChangeType(value, memberType));
            }

            return obj;
        }

        #endregion
    }
}