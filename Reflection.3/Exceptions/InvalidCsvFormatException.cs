﻿using System;

namespace OtusHomework.Reflection
{
    public class InvalidCsvFormatException : Exception
    {
        public InvalidCsvFormatException() : base("Invalid csv format.") { }
    }
}