﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace OtusHomework.Reflection
{
    class Program
    {
        private static void Main()
        {
            var f = new F().Get();
            var csv = Serializer.SerializeFromObjectToCSV(f);
            var json = JsonConvert.SerializeObject(f);
            
            Console.WriteLine("=== Serializing from object to csv ===");
            WriteFuncExecutionInfo(() => Serializer.SerializeFromObjectToCSV(f));
            Console.WriteLine("=== Serializing from object to json ===");
            WriteFuncExecutionInfo(() => JsonConvert.SerializeObject(f));
            
            Console.WriteLine("=== Deserializing from csv to object ===");
            WriteFuncExecutionInfo(() => Serializer.DeserializeFromCSVToObject(csv));
            Console.WriteLine("=== Deserializing from json to object ===");
            WriteFuncExecutionInfo(() => JsonConvert.DeserializeObject<F>(json));
        }

        private static void WriteFuncExecutionInfo<T>(Func<T> func)
        {
            var displayedResult = func();
            Console.WriteLine("Result of execution:");
            Console.WriteLine(displayedResult);
            Console.WriteLine($"Average execution time: {GetAverageExecutionTime(() => func())} ms.");
            Console.WriteLine();
        } 

        private static decimal GetAverageExecutionTime(Action action)
        {
            const int iterationCount = 100000;

            var watch = new Stopwatch();
            var allTime = TimeSpan.Zero;
            
            for (var i = 0; i < iterationCount; i++)
            {
                watch.Start();
                action();
                watch.Stop();

                allTime += watch.Elapsed;
                
                watch.Reset();
            }

            return (decimal)allTime.TotalMilliseconds / iterationCount;
        }
    }
}