﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Delegates.Watcher;

namespace Delegates
{
    class Program
    {
        private static readonly ConcurrentBag<string> Files = new();
        
        static async Task Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("No arguments.");
                return;
            }

            var path = args[0];

            using var searcher = new FileSearcher();
            searcher.FileFound += SearcherOnFileFound;
            searcher.Start(path);

            await Task.Delay(1000);
            
            searcher.Stop();

            if (!Files.IsEmpty)
            {
                var longestFileName = Files.GetMax(f => f.Length);
                Console.WriteLine($"Longest filename in folder: {longestFileName} ({longestFileName.Length} chars).");
            }
            else
            {
                Console.WriteLine($"No files were found in the folder.");
            }

            Console.ReadKey();
        }

        private static void SearcherOnFileFound(object sender, FileArgs args)
        {
            Console.WriteLine($"Found a file named {args.FileName}.");
            Files.Add(args.FileName);
        }
    }
}