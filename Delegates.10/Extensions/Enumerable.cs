﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Delegates
{
    public static class EnumerableExtensions
    {
        public static T GetMax<T>(this IEnumerable<T> source, Func<T, float> getParameter) where T : class
        {
            var sourceArr = source.ToArray();

            if (!sourceArr.Any())
            {
                throw new ArgumentException("Sequence is empty.");
            }

            var maxNumber = float.MinValue;
            T result = null;

            foreach (var value in sourceArr)
            {
                var currentNumber = getParameter(value);
                
                if (currentNumber >= maxNumber)
                {
                    result = value;
                    maxNumber = currentNumber;
                }
            }

            return result;
        }
    }
}