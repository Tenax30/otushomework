﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Delegates.Watcher
{
    public class FileSearcher : IDisposable
    {
        public event FileFoundHandler FileFound;

        private bool _searchInProgress;
        private readonly object _lock = new();
        private CancellationTokenSource _searchCts = new();

        public void Start(string path)
        {
            lock (_lock)
            {
                if (_searchInProgress)
                {
                    throw new Exception("The file search is already in progress.");
                }
                
                if (!Directory.Exists(path))
                {
                    throw new ArgumentException($"Directory with path {path} not found.");
                }

                _searchInProgress = true;
            }

            Task.Factory.StartNew(() => FindFiles(path, _searchCts.Token), TaskCreationOptions.LongRunning);
        }

        private void FindFiles(string path, CancellationToken token)
        {
            var foundFiles = new HashSet<string>();
            
            while (!token.IsCancellationRequested && _searchInProgress)
            {
                foreach (var fullFileName in Directory.GetFiles(path))
                {
                    var fileName = fullFileName.Replace(path, "");
                    
                    if (foundFiles.Add(fileName))
                    {
                        FileFound?.Invoke(this, new FileArgs(fileName));
                    }
                }
            }
        } 
        

        public void Stop()
        {
            lock (_lock)
            {
                if (!_searchInProgress)
                {
                    throw new Exception("No files are searched.");
                }
                
                _searchInProgress = false;
            }
        }

        public void Dispose()
        {
            _searchCts?.Cancel();
            _searchCts?.Dispose();
            _searchCts = null;
        }
    }
}