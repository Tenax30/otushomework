﻿using System;

namespace Delegates.Watcher
{
    public delegate void FileFoundHandler(object sender, FileArgs args);
    
    public class FileArgs : EventArgs
    {
        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
        
        public string FileName { get; }
    }
}