﻿using System;
using System.Collections.Generic;
using Database.Extensions;

namespace Database.DataGeneration
{
    public class PaymentGenerator
    {
        private const int MinPaymentNumber = 1;
        private const int MaxPaymentNumber = 5;

        private const int MinExpirationOffsetDays = 30;
        private const int MaxExpirationOffsetDays = 90;

        private const int SumCoefficient = 3;

        private readonly Random _rnd = new();
        
        public IEnumerable<PaymentEntity> Generate(AdEntity ad, int? paymentNumber = null)
        {
            if (paymentNumber < 1)
            {
                throw new ArgumentException("Invalid ad number.");
            }

            var resultUserNumber = paymentNumber ?? _rnd.Next(MinPaymentNumber, MaxPaymentNumber + 1);
            var result = new List<PaymentEntity>();
            
            for (var i = 0; i < resultUserNumber; i++)
            {
                result.Add(GeneratePayment(ad));
            }

            return result;
        }

        private PaymentEntity GeneratePayment(AdEntity ad)
        {
            return new()
            {
                Id = Guid.NewGuid(),
                ExpirationDate = GenerateExpirationDate(),
                Sum = GenerateSum(),
                Type = EnumHelper.GetRandomEnum<PaymentType>(),
                Ad = ad
            };
        }

        private DateTime? GenerateExpirationDate()
        {
            var isEmpty = Convert.ToBoolean(_rnd.Next(2));
            
            if (isEmpty)
            {
                return null;
            }
            
            var offset = _rnd.Next(MinExpirationOffsetDays, MaxExpirationOffsetDays + 1);
            return DateTime.Now.AddDays(offset);
        }
        
        private decimal GenerateSum()
        {
            return (decimal)(_rnd.Next(1, 10) * Math.Pow(10, _rnd.Next(SumCoefficient + 1)));
        }
    }
}