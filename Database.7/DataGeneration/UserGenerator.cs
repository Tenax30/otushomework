﻿using System;
using System.Collections.Generic;
using Database.Extensions;

namespace Database.DataGeneration
{
    public class UserGenerator
    {
        private const int MinUserNumber = 5;
        private const int MaxUserNumber = 10;

        private const int MinEmailLength = 5;
        private const int MaxEmailLength = 15;

        private const int MinPasswordLength = 8;
        private const int MaxPasswordLength = 15;
        
        private const char DefaultCountryCode = '7';

        private readonly string[] _firstNames = {"Олег", "Иван", "Евгений", "Егор", "Максим"};
        private readonly string[] _middleNames = {"Алексеевич", "Иванович", "Борисович", "Сергеевич", "Дмитриевич"};
        private readonly string[] _lastNames = {"Корнев", "Попов", "Волчев", "Климачев", "Лихачев"};

        private readonly Random _rnd = new();

        public IEnumerable<UserEntity> Generate(int? userNumber = null)
        {
            if (userNumber < 1)
            {
                throw new ArgumentException("Invalid user number.");
            }

            var resultUserNumber = userNumber ?? _rnd.Next(MinUserNumber, MaxUserNumber + 1);
            var result = new List<UserEntity>();
            
            for (var i = 0; i < resultUserNumber; i++)
            {
                result.Add(GenerateUser());
            }

            return result;
        }

        private UserEntity GenerateUser()
        {
            return new()
            {
                Id = Guid.NewGuid(),
                Email = $"{StringHelper.GetRandomString(MinEmailLength, MaxEmailLength)}@mail.com",
                PhoneNumber = StringHelper.GetRandomPhoneNumber(DefaultCountryCode),
                Password = StringHelper.GetRandomString(MinPasswordLength, MaxPasswordLength),
                FirstName = _firstNames[_rnd.Next(_firstNames.Length)],
                MiddleName = GenerateMiddleName(),
                LastName = _lastNames[_rnd.Next(_lastNames.Length)]
            };
        }

        private string GenerateMiddleName()
        {
            var isEmpty = Convert.ToBoolean(_rnd.Next(2));
            return isEmpty ? null : _middleNames[_rnd.Next(_middleNames.Length)];
        }
    }
}