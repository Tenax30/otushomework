﻿using System;
using System.Collections.Generic;

namespace Database.DataGeneration
{
    public class AdGenerator
    {
        private const int MinAdNumber = 1;
        private const int MaxAdNumber = 5;
        
        private const int MinStreetNumber = 1;
        private const int MaxStreetNumber = 100;
        private const int MinApartmentNumber = 1;
        private const int MaxApartmentNumber = 100;
        
        private const int PriceCoefficient = 5;

        private readonly string[] _streetNames = {"Рубенштейна", "Василеостровская", "Мончегорская", "Маршала Жукова"
            , "Обуховская"};

        private readonly string[] _titleFirstWords = {"Продам", "Отдам", "Сдам", "Обменяю"};
        private readonly string[] _titleSecondWords = {"квартиру", "кота", "компьютер", "одежду", "жену" };

        private readonly Random _rnd = new();
        
        public IEnumerable<AdEntity> Generate(UserEntity user, int? adNumber = null)
        {
            if (adNumber < 1)
            {
                throw new ArgumentException("Invalid ad number.");
            }

            var resultUserNumber = adNumber ?? _rnd.Next(MinAdNumber, MaxAdNumber + 1);
            var result = new List<AdEntity>();
            
            for (var i = 0; i < resultUserNumber; i++)
            {
                result.Add(GenerateAd(user));
            }

            return result;
        }

        private AdEntity GenerateAd(UserEntity user)
        {
            return new()
            {
                Id = Guid.NewGuid(),
                Address = GenerateAddress(),
                ContactNumber = new string(user.PhoneNumber),
                Title = GenerateTitle(),
                Price = GeneratePrice(),
                User = user
            };
        }

        private string GenerateAddress()
        {
            var streetName = _streetNames[_rnd.Next(_streetNames.Length)];
            var streetNumber = _rnd.Next(MinStreetNumber, MaxStreetNumber);
            var apartmentNumber = _rnd.Next(MinApartmentNumber, MaxApartmentNumber);

            return $"ул. {streetName} {streetNumber} кв. {apartmentNumber}";
        }

        private string GenerateTitle()
        {
            var firstWord = _titleFirstWords[_rnd.Next(_titleFirstWords.Length)];
            var secondWord = _titleSecondWords[_rnd.Next(_titleSecondWords.Length)];

            return $"{firstWord} {secondWord}";
        }

        private decimal GeneratePrice()
        {
            return (decimal)(_rnd.Next(1, 10) * Math.Pow(10, _rnd.Next(PriceCoefficient + 1)));
        }
    }
}