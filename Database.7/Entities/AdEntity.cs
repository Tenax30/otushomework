﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database
{
    public class AdEntity
    {
        public Guid Id { get; set; }
        
        [Required]
        [MaxLength(200)]
        public string Title { get; set; }
        
        [Column(TypeName = "text")]
        public string Description { get; set; }
        
        [Required]
        [MaxLength(200)]
        public string Address { get; set; }
        
        [Required]
        [Column(TypeName = "numeric(10,2)")]
        public decimal Price { get; set; }
        
        [Required]
        [MaxLength(11)]
        public string ContactNumber { get; set; }
        
        [Required]
        public UserEntity User { get; set; }

        public IList<PaymentEntity> Payments { get; set; } = new List<PaymentEntity>();
        
        public override string ToString()
        {
            return $"AD Id = {Id}, Title = {Title}, Description = {Description}, Address = {Address}" +
                   $", Price = {Price}, ContactNumber = {ContactNumber}, UserId = {User?.Id}";
        }
    }
}