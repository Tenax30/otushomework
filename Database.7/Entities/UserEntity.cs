﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Database
{
    public class UserEntity
    {
        public Guid Id { get; set; }
        
        [Required]
        [MaxLength(11)]
        public string PhoneNumber { get; set; }

        [MaxLength(150)]
        public string Email { get; set; }
        
        [Required]
        [MinLength(6)]
        [MaxLength(100)]
        public string Password { get; set; }

        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [MaxLength(100)]
        public string MiddleName { get; set; }
        
        [MaxLength(100)]
        public string LastName { get; set; }

        public IList<AdEntity> Ads { get; set; } = new List<AdEntity>();
        
        public override string ToString()
        {
            return $"USER Id = {Id}, PhoneNumber = {PhoneNumber}, Email = {Email}, Password = {Password}" +
                   $", FirstName = {FirstName}, MiddleName = {MiddleName}, LastName = {LastName}";
        }
    }
}