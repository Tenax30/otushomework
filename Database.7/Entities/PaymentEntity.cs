﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database
{
    public class PaymentEntity
    {
        public Guid Id { get; set; }
        
        [Required]
        [Column(TypeName = "numeric(10,2)")]
        public decimal Sum { get; set; }
        
        [Required]
        public PaymentType Type { get; set; }
        
        public DateTime? ExpirationDate { get; set; }
        
        [Required]
        public AdEntity Ad { get; set; }
        
        public override string ToString()
        {
            return $"PAYMENT Id = {Id}, Sum = {Sum}, Type = {Type.ToString()}, ExpirationDate = {ExpirationDate}" +
                   $", AdId = {Ad?.Id}";
        }
    }
}