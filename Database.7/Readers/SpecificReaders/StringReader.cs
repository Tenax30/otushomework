﻿using System;

namespace Database.Readers
{
    public class StringReader
    {
        public string Read(string paramName, bool allowEmpty, int minLength = 0, int maxLength = int.MaxValue)
        {
            CheckLengths(minLength, maxLength);
            
            while (true)
            {
                Console.Write($"{paramName}: ");
                var line = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(line))
                {
                    if (!IsValidLength(line, minLength, maxLength))
                    {
                        Console.WriteLine("Некорретная длина строки.");
                    }
                    else
                    {
                        return line;
                    }
                }
                else
                {
                    if (!allowEmpty)
                    {
                        Console.WriteLine("Введите непустой параметр.");
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        private static void CheckLengths(int minLength, int maxLength)
        {
            if (minLength < 0 || maxLength < 0 || maxLength < minLength)
            {
                throw new ArgumentException("Incorrect length parameters.");
            }
        }

        private static bool IsValidLength(string line, int minLength, int maxLength)
        {
            return line.Length >= minLength && line.Length <= maxLength;
        }
    }
}