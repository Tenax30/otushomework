﻿using System;
using System.Linq;

namespace Database.Readers
{
    public class PhoneReader
    {
        public string Read()
        {
            while (true)
            {
                Console.Write("Телефон: ");
                var phone = Console.ReadLine();
                
                if (IsValidPhone(phone))
                {
                    return phone;
                }

                Console.WriteLine("Некорректный номер телефона.");
            }
        }

        private static bool IsValidPhone(string phone)
        {
            const int phoneNumberLength = 11;
            return phone != null && phone.Length == phoneNumberLength && phone.All(char.IsDigit);
        }
    }
}