﻿using System;

namespace Database.Readers
{
    public class EnumReader
    {
        public T Read<T>(string paramName) where T: struct, Enum 
        {
            while (true)
            {
                Console.Write($"{paramName}: ");
                if (Enum.TryParse(Console.ReadLine(), true, out T result))
                {
                    return result;
                }

                Console.WriteLine("Некорректный параметр.");
            }
        }
    }
}