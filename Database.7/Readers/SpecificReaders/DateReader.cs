﻿using System;

namespace Database.Readers
{
    public class DateReader
    {
        public DateTime? Read(string paramName, bool allowEmpty)
        {
            while (true)
            {
                Console.Write($"{paramName}: ");

                var line = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(line))
                {
                    if (DateTime.TryParse(line, out var date))
                    {
                        return date;
                    }

                    Console.WriteLine("Некорректная дата.");
                }
                else
                {
                    if (!allowEmpty)
                    {
                        Console.WriteLine("Введите непустой параметр.");
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }  
    }
}