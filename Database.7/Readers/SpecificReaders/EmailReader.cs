﻿using System;
using System.Text.RegularExpressions;

namespace Database.Readers
{
    public class EmailReader
    {
        public string Read(bool allowEmpty, int maxLength)
        {
            CheckLength(maxLength);
            
            while (true)
            {
                Console.Write("Эл. почта: ");
                var email = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(email))
                {
                    if (email.Length > maxLength)
                    {
                        Console.WriteLine("Некорректная длина электронной почты.");
                    }
                    else if (!IsValidEmail(email))
                    {
                        Console.WriteLine("Невалидная почта.");
                    }
                    else
                    {
                        return email;
                    }
                }
                else
                {
                    if (!allowEmpty)
                    {
                        Console.WriteLine("Введите непустой параметр.");
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        private static bool IsValidEmail(string email)
        {
            const string pattern = "[.\\-_a-z0-9]+@([a-z0-9][\\-a-z0-9]+\\.)+[a-z]{2,6}";
            var isMatch = Regex.Match(email, pattern, RegexOptions.IgnoreCase);
            return isMatch.Success;
        }

        private static void CheckLength(int maxLength)
        {
            if (maxLength < 0)
            {
                throw new ArgumentException("Incorrect length parameters.");
            }
        }
    }
}