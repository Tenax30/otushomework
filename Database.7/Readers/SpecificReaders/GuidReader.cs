﻿using System;

namespace Database.Readers
{
    public class GuidReader
    {
        public Guid Read(string paramName)
        {
            while (true)
            {
                Console.Write($"{paramName}: ");
                var line = Console.ReadLine();

                if (!Guid.TryParse(line, out var result))
                {
                    Console.WriteLine("Некорректный Id.");
                }
                else
                {
                    return result;
                }
            }
        }
    }
}