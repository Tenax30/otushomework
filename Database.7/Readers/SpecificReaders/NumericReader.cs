﻿using System;

namespace Database.Readers
{
    public class NumericReader
    {
        public decimal Read(string paramName)
        {
            while (true)
            {
                Console.Write($"{paramName}: ");
                var line = Console.ReadLine();

                if (decimal.TryParse(line, out var result))
                {
                    return result;
                }
                
                Console.WriteLine("Некорректный параметр.");
            }
        }
    }
}