﻿using System;

namespace Database.Readers
{
    public class Reader
    {
        private readonly StringReader _stringReader;
        private readonly PhoneReader _phoneReader;
        private readonly EmailReader _emailReader;
        private readonly NumericReader _numericReader;
        private readonly EnumReader _enumReader;
        private readonly DateReader _dateReader;
        private readonly GuidReader _guidReader;
        
        public Reader()
        {
            _stringReader = new StringReader();
            _phoneReader = new PhoneReader();
            _emailReader = new EmailReader();
            _numericReader = new NumericReader();
            _enumReader = new EnumReader();
            _dateReader = new DateReader();
            _guidReader = new GuidReader();
        }
        
        public string ReadString(string paramName, bool allowEmpty, int minLength = 0, int maxLength = int.MaxValue)
        {
            return _stringReader.Read(paramName, allowEmpty, minLength, maxLength);
        }
        
        public string ReadPhone()
        {
            return _phoneReader.Read();
        }
        
        public string ReadEmail(bool allowEmpty, int maxLength)
        {
            return _emailReader.Read(allowEmpty, maxLength);
        }
        
        public decimal ReadNumeric(string paramName)
        {
            return _numericReader.Read(paramName);
        }

        public T ReadEnum<T>(string paramName) where T : struct, Enum
        {
            return _enumReader.Read<T>(paramName);
        }
        
        public DateTime? ReadDate(string paramName, bool allowEmpty)
        {
            return _dateReader.Read(paramName, allowEmpty);
        }
        
        public Guid ReadGuid(string paramName)
        {
            return _guidReader.Read(paramName);
        }
    }
}