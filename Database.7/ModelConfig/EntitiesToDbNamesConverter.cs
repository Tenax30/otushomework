﻿using System;
using System.Text;

namespace Database
{
    public static class EntitiesToDbNamesConverter
    {
        public static string ConvertColumnName(string name)
        {
            return GetNameInSnakeCase(name);
        }
        
        public static string ConvertTableName(string name)
        {
            return GetNameInSnakeCase(name.Remove(name.IndexOf("Entity", StringComparison.Ordinal)));
        }
        
        private static string GetNameInSnakeCase(string name)
        {
            var result = new StringBuilder();
            
            for (var i = 0; i < name.Length; i++)
            {
                var character = name[i];
                
                if (char.IsUpper(character) && i != 0)
                {
                    result.Append('_');
                }

                result.Append(character);
            }

            return result.ToString().ToLower();
        }
    }
}