﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Database.Storage
{
    public class UserStorage
    {
        public async Task AddAsync(UserEntity user)
        {
            await using var db = new AvitoContext();
            await db.Users.AddAsync(user);
            await db.SaveChangesAsync();
        }
        
        public async Task AddAsync(IEnumerable<UserEntity> users)
        {
            await using var db = new AvitoContext();
            await db.Users.AddRangeAsync(users);
            await db.SaveChangesAsync();
        }
        
        public async Task<UserEntity> GetAsync(Guid id)
        {
            await using var db = new AvitoContext();
            return db.Users.FirstOrDefault(u => u.Id == id);
        }
        
        public async Task<int> GetCountAsync()
        {
            await using var db = new AvitoContext();
            return db.Users.Count();
        }

        public async Task<IEnumerable<UserEntity>> GetAllAsync(bool includeAds = false)
        {
            await using var db = new AvitoContext();
            var query = db.Users.AsQueryable();

            if (includeAds)
            {
                query = query.Include(u => u.Ads);
            }

            return query.ToArray();
        }
    }
}