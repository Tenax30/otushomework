﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Database.Storage
{
    public class AdStorage
    {
        public async Task AddAsync(AdEntity ad, Guid userId)
        {
            await using var db = new AvitoContext();

            var user = db.Users.FirstOrDefault(a => a.Id == userId);

            if (user == null)
            {
                throw new Exception($"Не удалось найти пользователя с ID {userId}.");
            }

            ad.User = user;
            
            await db.Ads.AddAsync(ad);
            await db.SaveChangesAsync();
        }

        public async Task<AdEntity> GetAsync(Guid id)
        {
            await using var db = new AvitoContext();
            return db.Ads.FirstOrDefault(a => a.Id == id);
        }
        
        public async Task<IEnumerable<AdEntity>> GetAllAsync(bool includeUser = false, bool includePayments = false)
        {
            await using var db = new AvitoContext();
            var query = db.Ads.AsQueryable();

            if (includeUser)
            {
                query = query.Include(a => a.User);
            }

            if (includePayments)
            {
                query = query.Include(a => a.Payments);
            }

            return query.ToArray();
        }
    }
}