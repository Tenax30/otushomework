﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Database.Storage
{
    public class PaymentStorage
    {
        public async Task AddPaymentAsync(PaymentEntity payment, Guid adId)
        {
            await using var db = new AvitoContext();

            var ad = db.Ads.FirstOrDefault(a => a.Id == adId);

            if (ad == null)
            {
                throw new Exception($"Объявление с ID {adId} не найдено.");
            }

            payment.Ad = ad;
            
            await db.Payments.AddAsync(payment);
            await db.SaveChangesAsync();
        }
        
        public async Task<IEnumerable<PaymentEntity>> GetAllAsync(bool includeAd = false)
        {
            await using var db = new AvitoContext();
            var query = db.Payments.AsQueryable();

            if (includeAd)
            {
                query = query.Include(p => p.Ad);
            }

            return query.ToArray();
        }
    }
}