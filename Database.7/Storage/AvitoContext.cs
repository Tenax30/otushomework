﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Database.Storage
{
    public class AvitoContext : DbContext
    {
        public DbSet<AdEntity> Ads { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<PaymentEntity> Payments { get; set; }

        public AvitoContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                "Host=localhost;Port=5432;Database=avito_homework;Username=postgres;Password=root");
            optionsBuilder.EnableSensitiveDataLogging();
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SetEntityNames(modelBuilder.Model.GetEntityTypes());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

        private static void SetEntityNames(IEnumerable<IMutableEntityType> entities)
        {
            foreach (var entity in entities)
            {
                entity.SetTableName(EntitiesToDbNamesConverter.ConvertTableName(entity.GetDefaultTableName()));
                
                foreach (var prop in entity.GetProperties())
                {
                    prop.SetColumnName(EntitiesToDbNamesConverter.ConvertColumnName(prop.GetColumnBaseName()));
                }
            }
        }
    }
}