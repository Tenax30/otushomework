﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.DataGeneration;

namespace Database.Storage
{
    public class StorageFacade
    {
        private readonly UserStorage _userStorage;
        private readonly AdStorage _adStorage;
        private readonly PaymentStorage _paymentStorage;
        
        public StorageFacade()
        {
            _userStorage = new UserStorage();
            _adStorage = new AdStorage();
            _paymentStorage = new PaymentStorage();
        }

        #region DataInitialization

        public async Task AddInitialDataAsync()
        {
            if (await _userStorage.GetCountAsync() > 0)
            {
                return;
            }
            
            var userGenerator = new UserGenerator();
            var adGenerator = new AdGenerator();
            var paymentGenerator = new PaymentGenerator();
            
            var users = userGenerator.Generate().ToArray();

            foreach (var user in users)
            {
                var ads = adGenerator.Generate(user).ToArray();
                user.Ads = ads;
                    
                foreach (var ad in ads)
                {
                    ad.Payments = paymentGenerator.Generate(ad).ToArray();
                }
            }

            await _userStorage.AddAsync(users);
        }

        #endregion

        #region Users

        public async Task AddUserAsync(UserEntity user)
        {
            await _userStorage.AddAsync(user);
        }
        
        public async Task<UserEntity> GetUserNoThrowAsync(Guid id)
        {
            return await _userStorage.GetAsync(id);
        }

        public async Task<IEnumerable<UserEntity>> GetAllUsersAsync(bool includeAds = false)
        {
            return await _userStorage.GetAllAsync(includeAds);
        }

        #endregion

        #region Ads

        public async Task AddAdAsync(AdEntity ad, Guid userId)
        {
            await _adStorage.AddAsync(ad, userId);
        }

        public async Task<AdEntity> GetAdAsync(Guid id)
        {
            return await _adStorage.GetAsync(id);
        }
        
        public async Task<IEnumerable<AdEntity>> GetAllAdsAsync(bool includeUser = false, bool includePayments = false)
        {
            return await _adStorage.GetAllAsync(includeUser, includePayments);
        }

        #endregion

        #region Payments

        public async Task AddPaymentAsync(PaymentEntity payment, Guid adId)
        {
            await _paymentStorage.AddPaymentAsync(payment, adId);
        }
        
        public async Task<IEnumerable<PaymentEntity>> GetAllPaymentsAsync(bool includeAd = false)
        {
            return await _paymentStorage.GetAllAsync(includeAd);
        }

        #endregion
    }
}