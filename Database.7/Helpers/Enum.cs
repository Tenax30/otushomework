﻿using System;

namespace Database.Extensions
{
    public static class EnumHelper
    {
        public static T GetRandomEnum<T>() where T: Enum
        {
            var values = Enum.GetValues(typeof(T));
            var rnd = new Random();
            
            return (T)values.GetValue(rnd.Next(values.Length));
        }
    }
}