﻿using System;

namespace Database.Extensions
{
    public static class StringHelper
    {
        private const string Letters = "qwertyuiopasdfghjklzxcvbnm";
        private const string Digits = "0123456789";
        
        public static string GetRandomString(int minLength, int maxLength)
        {
            if (minLength < 1 || maxLength < 1 || maxLength < minLength)
            {
                throw new ArgumentException("Invalid parameters.");
            }

            var rnd = new Random();
            var length = rnd.Next(minLength, maxLength + 1);
            var result = new char[length];

            const string symbols = Letters + Digits;

            for (var i = 0; i < length; i++)
            {
                result[i] = symbols[rnd.Next(symbols.Length)];
            }

            return new string(result);
        }

        public static string GetRandomPhoneNumber(char? countryCode = null)
        {
            if(countryCode.HasValue && !char.IsDigit(countryCode.Value))
            {
                throw new ArgumentException("Invalid country code.");
            }
            
            const int numberLength = 11;
            var result = new char[numberLength];
            var rnd = new Random();

            if (countryCode.HasValue)
            {
                result[0] = countryCode.Value;
            }
            else
            {
                result[0] = Digits[rnd.Next(Digits.Length)];
            }
            
            for (var i = 1; i < numberLength; i++)
            {
                result[i] = Digits[rnd.Next(Digits.Length)];
            }

            return new string(result);
        }
    }
}