﻿using System;
using System.Threading.Tasks;
using Database.Readers;
using Database.Storage;

namespace Database
{
    class Program
    {
        private static async Task Main()
        {
            var storage = new StorageFacade();
            // await storage.AddInitialDataAsync();

            while (true)
            {
                Console.WriteLine("Выберите действие: ");
                Console.WriteLine("1. Показать все данные из базы данных.");
                Console.WriteLine("2. Добавить нового пользователя.");
                Console.WriteLine("3. Добавить новое объявление.");
                Console.WriteLine("4. Добавить новый платеж.");
                Console.WriteLine("5. Завершить программу.");
                var line = Console.ReadLine();

                if (!Enum.TryParse(line, true, out UserAction action))
                {
                    Console.WriteLine("Некорректный ввод.");
                    continue;
                }
                
                switch (action)
                {
                    case UserAction.ShowAllData:
                        await ShowAllDataAsync(storage);
                        break;
                    case UserAction.AddUser:
                        await AddUserAsync(storage);
                        break;
                    case UserAction.AddAd:
                        await AddAdAsync(storage);
                        break;
                    case UserAction.AdPayment:
                        await AddPaymentAsync(storage);
                        break;
                    case UserAction.Exit:
                        return;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(action));
                }
            }
        }

        private static async Task ShowAllDataAsync(StorageFacade storage)
        {
            foreach (var user in await storage.GetAllUsersAsync(true))
            {
                Console.WriteLine(user.ToString());
            }

            foreach (var ad in await storage.GetAllAdsAsync(includeUser: true, true))
            {
                Console.WriteLine(ad.ToString());
            }

            foreach (var payment in await storage.GetAllPaymentsAsync(includeAd: true))
            {
                Console.WriteLine(payment.ToString());
            }
        }

        private static async Task AddUserAsync(StorageFacade storage)
        {
            var reader = new Reader();

            var user = new UserEntity
            {
                Id = Guid.NewGuid(),
                PhoneNumber = reader.ReadPhone(),
                Email = reader.ReadEmail(allowEmpty: true, maxLength: 150),
                Password = reader.ReadString(paramName: "Пароль", allowEmpty: false, minLength: 6, maxLength: 100),
                FirstName = reader.ReadString(paramName: "Имя", allowEmpty: false, maxLength: 100),
                MiddleName = reader.ReadString(paramName: "Фамилия", allowEmpty: true, maxLength: 100),
                LastName = reader.ReadString(paramName: "Отчество", allowEmpty: true, maxLength: 100),
            };

            await storage.AddUserAsync(user);
        }
        
        private static async Task AddAdAsync(StorageFacade storage)
        {
            var reader = new Reader();

            var userId = reader.ReadGuid("Id пользователя");
            var user = await storage.GetUserNoThrowAsync(userId);
            
            if (user == null)
            {
                Console.WriteLine("Пользователь с данным Id не найден.");
                return;
            }
            
            var ad = new AdEntity
            {
                Id = Guid.NewGuid(),
                Title = reader.ReadString(paramName: "Заголовок", allowEmpty: false, maxLength: 200),
                Description = reader.ReadString(paramName: "Описание", allowEmpty: true),
                Address = reader.ReadString(paramName: "Адрес", allowEmpty: true, maxLength: 200),
                Price = reader.ReadNumeric(paramName: "Цена"),
                ContactNumber = reader.ReadPhone(),
                User = user
            };

            await storage.AddAdAsync(ad, userId);
        }
        
        private static async Task AddPaymentAsync(StorageFacade storage)
        {
            var reader = new Reader();

            var adId = reader.ReadGuid("Id объявления");
            var ad = await storage.GetAdAsync(adId);
            
            if (ad == null)
            {
                Console.WriteLine("Объявление с данным Id не найдено.");
                return;
            }
            
            var payment = new PaymentEntity
            {
                Id = Guid.NewGuid(),
                Sum = reader.ReadNumeric(paramName: "Сумма"),
                Type = reader.ReadEnum<PaymentType>(paramName: "Тип платежа"),
                ExpirationDate = reader.ReadDate(paramName: "Дата окончания действия", allowEmpty: true),
                Ad = ad
            };

            await storage.AddPaymentAsync(payment, adId);
        }
    }
}