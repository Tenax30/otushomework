CREATE TABLE public.ad (
    id uuid NOT NULL,
    title character varying(200) NOT NULL,
    description text,
    address character varying(200) NOT NULL,
    price numeric(10,2) NOT NULL,
    contact_number character varying(11) NOT NULL,
    user_id uuid NOT NULL
);


CREATE TABLE public.payment (
    id uuid NOT NULL,
    sum numeric(10,2) NOT NULL,
    type integer NOT NULL,
    expiration_date timestamp without time zone,
    ad_id uuid NOT NULL
);


CREATE TABLE public."user" (
    id uuid NOT NULL,
    phone_number character varying(11) NOT NULL,
    email character varying(150),
    password character varying(100) NOT NULL,
    first_name character varying(100) NOT NULL,
    middle_name character varying(100),
    last_name character varying(100)
);

ALTER TABLE ONLY public.ad
    ADD CONSTRAINT "PK_ad" PRIMARY KEY (id);
ALTER TABLE ONLY public.payment
    ADD CONSTRAINT "PK_payment" PRIMARY KEY (id);
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_user" PRIMARY KEY (id);

CREATE INDEX "IX_ad_user_id" ON public.ad USING btree (user_id);
CREATE INDEX "IX_payment_ad_id" ON public.payment USING btree (ad_id);
CREATE UNIQUE INDEX "IX_user_email" ON public."user" USING btree (email);
CREATE UNIQUE INDEX "IX_user_phone_number" ON public."user" USING btree (phone_number);

ALTER TABLE ONLY public.ad
    ADD CONSTRAINT "FK_ad_user_user_id" FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;
ALTER TABLE ONLY public.payment
    ADD CONSTRAINT "FK_payment_ad_ad_id" FOREIGN KEY (ad_id) REFERENCES public.ad(id) ON DELETE CASCADE;
